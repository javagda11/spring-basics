package com.sda.cwiczenia.repository;

import com.sda.cwiczenia.model.Dziennik;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DziennikRepository extends JpaRepository<Dziennik, Long> {

}
