package com.sda.cwiczenia.repository;

import com.sda.cwiczenia.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    // select * from students where pesel = 1?
    Optional<Student> findByPesel(String pesel);
}
