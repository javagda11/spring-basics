package com.sda.cwiczenia.controller;

import com.sda.cwiczenia.model.Ocena;
import com.sda.cwiczenia.model.dto.AddGradeDto;
import com.sda.cwiczenia.service.OcenaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class OcenaController {

    @Autowired
    private OcenaService ocenaService;

    @PostMapping(path = "/dodajOcene")
    public ResponseEntity dodajOcene(@RequestBody AddGradeDto addGradeDto){
        Optional<Ocena> optionalOcena = ocenaService.dodajOcene(addGradeDto);
        if(optionalOcena.isPresent()){
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }
}
