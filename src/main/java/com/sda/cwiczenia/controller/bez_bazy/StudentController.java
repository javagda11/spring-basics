package com.sda.cwiczenia.controller.bez_bazy;

import com.sda.cwiczenia.model.Student;
import com.sda.cwiczenia.service.bez_bazy.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/staryStudent/")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(path = "/dodajStudenta", method = RequestMethod.POST)
    public ResponseEntity dodajStudenta(@RequestBody Student student) {
        studentService.dodajStudenta(student);

        return ResponseEntity.ok(student);
    }

    @RequestMapping(path = "/usunStudenta/{parametr}")
    public ResponseEntity usunStudenta(@PathVariable(name = "parametr") Long id) {
        boolean udaloSie = studentService.usunStudenta(id);

        if (udaloSie) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(path = "/edytujStudenta/{id_studenta}", method = RequestMethod.POST)
    public ResponseEntity edytujStudenta(@PathVariable(name = "id_studenta") Long id,
                                         @RequestBody Student student) {

        Optional<Student> studentOptional = studentService.edytujStudenta(id, student);

        // jeśli mamy studenta w wyniku metody
        if (studentOptional.isPresent()) {
            // wynikiem metody jest wyedytowany student
            return ResponseEntity.ok(studentOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    // - pobierz studenta po id - mapping który przyjmuje pathvariable id studenta i
    //                              zwraca tego studenta.
    @GetMapping(path = "/pobierzStudenta/{id_stud}")
    public ResponseEntity pobierzStudenta(@PathVariable(name = "id_stud") Long id) {
        Optional<Student> studentOptional = studentService.znajdzStudentaNaLiscie(id);

        // jeśli mamy studenta w wyniku metody
        if (studentOptional.isPresent()) {
            // wynikiem metody jest wyedytowany student
            return ResponseEntity.ok(studentOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    // - pobierz listę studentów - mapping który nie przyjmuje żadnego parametru i zwraca
    //                              listę studentów
    @GetMapping(path = "/pobierzListe")
    public ResponseEntity pobierzListe() {
        return ResponseEntity.ok(studentService.listaStudentow());
    }

    // - pobierz studentów urodzonych w konkretnym roku -
    //                              mapping który jako parametr przyjmuje rok urodzenia
    //                              (wystarczą dwie ostatnie cyfry -
    //                              nie uwzględniamy roku po 2000)
    @GetMapping(path = "/pobierzPoRokuUrodzenia/{rok_ur}")
    public ResponseEntity pobierzPoRokuUrodzenia(@PathVariable(name = "rok_ur") String rok) {
        List<Student> studentList = studentService.znajdzStudentaUrodzonegoW(rok);

        return ResponseEntity.ok(studentList);
    }
}
