package com.sda.cwiczenia.controller.bez_bazy;

import com.sda.cwiczenia.model.Dziennik;
import com.sda.cwiczenia.service.bez_bazy.DziennikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/staryDziennik/")
public class DziennikController {

    @Autowired
    private DziennikService dziennikService;

    @RequestMapping(path = "/dodajDziennik", method = RequestMethod.POST)
    public ResponseEntity dodajDziennik(@RequestBody Dziennik dziennik) {
        dziennikService.dodajDziennik(dziennik);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/listaDziennikow", method = RequestMethod.GET)
    public ResponseEntity listaDziennikow() {
        return ResponseEntity.ok(dziennikService.zwrocListe());
    }

    @RequestMapping(path = "/dziennikId/{id_dziennika}", method = RequestMethod.GET)
    public ResponseEntity dziennikOId(@PathVariable(name = "id_dziennika") Long id) {
        Optional<Dziennik> optionalDziennik = dziennikService.znajdzDziennik(id);
        if (optionalDziennik.isPresent()) {
            return ResponseEntity.ok(optionalDziennik.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(path = "/usunDziennik/{id_dziennika}")
    public ResponseEntity usunDziennik(@PathVariable(name = "id_dziennika") Long id) {
        boolean sukces = dziennikService.usunDziennik(id);
        if (sukces) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.badRequest().build();
    }
}
