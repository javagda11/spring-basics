package com.sda.cwiczenia.controller;

import com.sda.cwiczenia.service.MemoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MemoryController {

    @Autowired
    private MemoryService memoryService;

    @GetMapping("/dodajLiczbe")
    public ResponseEntity dodajLiczbe(
            @RequestParam(name = "doDodania") int doDodania) {
        memoryService.dodajLiczbe(doDodania);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/podajLiczby")
    public ResponseEntity podajLiczby() {
        return ResponseEntity.ok(memoryService.zwrocLiczby());
    }

    @GetMapping("/podajNajwieksza")
    public ResponseEntity podajNajwieksza() {
        return ResponseEntity.ok(memoryService.najwieksza());
    }

    @GetMapping("/podajNajmniejsza")
    public ResponseEntity podajNajmniejsza() {
        return ResponseEntity.ok(memoryService.najmniejsza());
    }

    @GetMapping("/wyczyscLiczby")
    public ResponseEntity wyczyscLiczby() {
        memoryService.wyczysc();
        return ResponseEntity.ok().build();
    }

    @GetMapping("/zwrocPosortowane")
    public ResponseEntity zwrocPosrotowane() {
        return ResponseEntity.ok(memoryService.posortowane());
    }

    // #########################################################################
    @GetMapping("/dodajSlowo")
    public ResponseEntity dodajSlowo(@RequestParam(name = "slowo") String slowo) {
        memoryService.dodajSlowo(slowo);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/listujSlowa")
    public ResponseEntity listujSlowa() {
        return ResponseEntity.ok(memoryService.listaSlow());
    }

    @GetMapping("/listujPalindromy")
    public ResponseEntity listujPalindromy() {
        return ResponseEntity.ok(memoryService.listaPalindromow());
    }

    @GetMapping("/najkrotsze")
    public ResponseEntity najkrotsze() {
        return ResponseEntity.ok(memoryService.najkrotsze());
    }

    @GetMapping("/zWielkiej")
    public ResponseEntity zWielkiej() {
        return ResponseEntity.ok(memoryService.zwrocZWielkiej());
    }
}
