package com.sda.cwiczenia.controller;

import com.sda.cwiczenia.model.Dziennik;
import com.sda.cwiczenia.model.dto.AddStudentToDziennik;
import com.sda.cwiczenia.service.DziennikServiceBD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class DziennikControllerBD {

    @Autowired
    private DziennikServiceBD dziennikService;

    @PostMapping(path = "/dodajDziennik")
    public ResponseEntity dodajDziennik(@RequestBody Dziennik dziennik) {
        Dziennik dziennik1 = dziennikService.dodajDziennik(dziennik);

        return ResponseEntity.ok(dziennik);
    }

    @GetMapping(path = "/dodajStudentaDoDziennika/{id_dziennik}/{id_student}")
    public ResponseEntity dodajStudentaDoDziennika(@PathVariable(name = "id_dziennik") Long id_dziennik,
                                                   @PathVariable(name = "id_student") Long id_student) {
        Optional<Dziennik> optionalDziennik = dziennikService.dodajStudentaDoDziennika(id_dziennik, id_student);

        if (optionalDziennik.isPresent()) {
            return ResponseEntity.ok(optionalDziennik.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping(path = "/dodajStudentaDoDziennika")
    public ResponseEntity dodajStudentaDoDziennika(
            @RequestBody AddStudentToDziennik addStudentToDziennik) {
        Optional<Dziennik> optionalDziennik = dziennikService
                .dodajStudentaDoDziennika(
                        addStudentToDziennik.getDziennik_id(),
                        addStudentToDziennik.getStudent_id());

        if (optionalDziennik.isPresent()) {
            return ResponseEntity.ok(optionalDziennik.get());
        }
        return ResponseEntity.badRequest().build();
    }

}





