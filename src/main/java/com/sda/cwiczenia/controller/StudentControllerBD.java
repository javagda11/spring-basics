package com.sda.cwiczenia.controller;

import com.sda.cwiczenia.model.Student;
import com.sda.cwiczenia.model.dto.AddStudentDto;
import com.sda.cwiczenia.model.dto.EditStudentDto;
import com.sda.cwiczenia.service.StudentServiceBD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class StudentControllerBD {

    @Autowired
    private StudentServiceBD studentServiceBD;

    @GetMapping(path = "/listaStudentow")
    public ResponseEntity listaStudentow() {
        return ResponseEntity.ok(studentServiceBD.pobierzListeStudentow());
    }

    @GetMapping(path = "/usunStudenta/{id_studenta}")
    public ResponseEntity usunStudenta(@PathVariable(name = "id_studenta") Long id) {
        boolean sukces = studentServiceBD.usunStudenta(id);
        if (sukces) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @PostMapping(path = "/dodajStudenta")
    public ResponseEntity dodajStudenta(@RequestBody AddStudentDto student) {
        Student dodany = studentServiceBD.zapiszStudenta(student);

        return ResponseEntity.ok(student);
    }

    @PostMapping(path = "/edytuj/{id_studenta}")
    public ResponseEntity edytuj(@PathVariable(name = "id_studenta") Long id,
                                 @RequestBody EditStudentDto student) {
        Optional<Student> edytowanyStudent = studentServiceBD.aktualizuj(id, student);

        if (edytowanyStudent.isPresent()) {
            return ResponseEntity.ok(edytowanyStudent.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @GetMapping(path = "/szukajPoPeselu")
    public ResponseEntity szukajPoPeselu(@RequestParam(name = "pesel") String pesel) {
        Optional<Student> szukanyStudent = studentServiceBD.szukajPoPeselu(pesel);

        if (szukanyStudent.isPresent()) {
            return ResponseEntity.ok(szukanyStudent.get());
        }
        return ResponseEntity.badRequest().build();
    }


    // - dodawanie
    // - szukanie studenta po id
    // -* szukanie studentów po peselu

}
