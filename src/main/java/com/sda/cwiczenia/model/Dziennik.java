package com.sda.cwiczenia.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dziennik {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany
    private List<Student> listaUczniow;

    private String nazwaklasy;
    private String nazwaSzkoly;
    private int rocznik; // int - np. 1995
    private String nazwiskoWychowawcy;
}
