package com.sda.cwiczenia.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddGradeDto {
    private Long studentId;
    private Integer grade;
    private String subject;
}
