package com.sda.cwiczenia.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddStudentToDziennik {
    private Long student_id;
    private Long dziennik_id;

}
