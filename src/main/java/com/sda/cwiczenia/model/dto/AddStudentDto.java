package com.sda.cwiczenia.model.dto;

import com.sda.cwiczenia.model.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddStudentDto {
    private String imie;
    private String nazwisko;
    private String pesel;

    @NonNull
    private String indeks;

    public static Student createFromDto(AddStudentDto dto){
        return new Student(dto.getImie(), dto.getNazwisko(), dto.getPesel(), dto.getIndeks());
    }
}
