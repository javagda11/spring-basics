package com.sda.cwiczenia.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data

public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String imie;
    private String nazwisko;
    private String pesel;
    private String indeks;

    @OneToMany
    private List<Ocena> ocenas;

    public Student() {
    }

    public Student(String imie, String nazwisko, String pesel, String indeks, List<Ocena> ocenas) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.indeks = indeks;
        this.ocenas = ocenas;
    }

    public Student(String imie, String nazwisko, String pesel, String indeks) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
        this.indeks = indeks;
    }
}
