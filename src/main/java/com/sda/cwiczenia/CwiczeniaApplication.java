package com.sda.cwiczenia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CwiczeniaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CwiczeniaApplication.class, args);
	}
}
