package com.sda.cwiczenia.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class MemoryService {

    private List<Integer> list = new ArrayList<>();
    private List<String> listaSlow = new ArrayList<>();

    public void dodajLiczbe(Integer liczba) {
        list.add(liczba);
    }

    public List<Integer> zwrocLiczby() {
        return list;
    }

    public void wyczysc() {
        list.clear();
    }

    public int najwieksza() {
        int max = list.get(0);
        for (int i = 0; i < list.size(); i++) {
            if (max < list.get(i)) {
                max = list.get(i);
            }
        }
        return max;
    }

    public int najmniejsza() {
        int min = list.get(0);
        for (int i = 0; i < list.size(); i++) {
            if (min > list.get(i)) {
                min = list.get(i);
            }
        }
        return min;
    }

    public List<Integer> posortowane() {
        List<Integer> kopia = new ArrayList<>(list); // tworze kopie listy
        Collections.sort(kopia);        // sortuje kopie

        return kopia; // zwracam kopie
    }

    //##########################################################################
    public String najkrotsze() {
        String najkrotsze = listaSlow.get(0);
        for (int i = 0; i < listaSlow.size(); i++) {
            if (najkrotsze.length() > listaSlow.get(i).length()) {
                najkrotsze = listaSlow.get(i);
            }
        }
        return najkrotsze;
    }

    public List<String> zwrocZWielkiej() {
        List<String> zWielkiej = new ArrayList<>();
        for (String slowo : listaSlow) {
            if (czyZaczynaSieZWielkiejRegexp(slowo)) {
                zWielkiej.add(slowo);
            }
        }
        return zWielkiej;
    }

    public boolean czyZaczynaSieZWielkiejRegexp(String slowo) {
        if (slowo.matches("^[A-Z].*")) {
            return true;
        }
        return false;
    }

    public boolean czyZaczynaSieZWielkiej(String slowo) {
        if (slowo.charAt(0) >= 65 || slowo.charAt(0) <= 90) {
            return true;
        }
        return false;
    }

    public List<String> listaPalindromow() {
        List<String> palindromy = new ArrayList<>();
        for (String slowo : listaSlow) {
            if (jestPalindromem(slowo)) {
                palindromy.add(slowo);
            }
        }
        return palindromy;
    }

    public boolean jestPalindromem(String slowo) {
        String sprawdzane = slowo.replace(" ", "");
        sprawdzane = sprawdzane.toLowerCase();

        for (int i = 0; i < sprawdzane.length() / 2; i++) {
            if (sprawdzane.charAt(i) != sprawdzane.charAt(sprawdzane.length() - i - 1)) {
                return false;
            }
        }
        return true;
    }

    public void dodajSlowo(String slowo) {
        listaSlow.add(slowo);
    }

    public List<String> listaSlow() {
        return listaSlow;
    }
}
