package com.sda.cwiczenia.service;

import com.sda.cwiczenia.model.Ocena;
import com.sda.cwiczenia.model.Przedmiot;
import com.sda.cwiczenia.model.Student;
import com.sda.cwiczenia.model.dto.AddGradeDto;
import com.sda.cwiczenia.repository.OcenaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OcenaService {

    @Autowired
    private OcenaRepository ocenaRepository;

    @Autowired
    private StudentServiceBD studentServiceBD;

    public Optional<Ocena> dodajOcene(AddGradeDto dto) {
        Optional<Student> optionalStudent = studentServiceBD.znajdzPoId(dto.getStudentId());
        if (optionalStudent.isPresent()) {
            Student student = optionalStudent.get();

            Przedmiot przedmiot = Przedmiot.valueOf(dto.getSubject());

            // po stworzeniu oceny muszę ją najpierw dodać do bazy danych
            Ocena ocena = new Ocena(null, dto.getGrade(), przedmiot);
            ocena = ocenaRepository.save(ocena);

            // dopiero po dodaniu do bazy mogę ją dodać jako ocenę uczniowi
            student.getOcenas().add(ocena);
            studentServiceBD.zapisz(student);

            return Optional.of(ocena);
        }

        return Optional.empty();
    }
}
