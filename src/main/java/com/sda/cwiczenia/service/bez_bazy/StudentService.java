package com.sda.cwiczenia.service.bez_bazy;

import com.sda.cwiczenia.model.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private List<Student> list = new ArrayList<>();

    public void dodajStudenta(Student student) {
        list.add(student);
    }

    public boolean usunStudenta(Long id) {
        Optional<Student> studentOptional = znajdzStudentaNaLiscie(id);
        if (studentOptional.isPresent()) {
            list.remove(studentOptional.get());
            return true;
        }

        return false;
    }

    public Optional<Student> znajdzStudentaNaLiscie(Long id) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == id) {
                return Optional.of(list.get(i));
            }
        }
        return Optional.empty();
    }

    public Optional<Student> edytujStudenta(Long id, Student student) {
        Optional<Student> studentOptional = znajdzStudentaNaLiscie(id);
        if (studentOptional.isPresent()) {
            Student doEdycji = studentOptional.get();

            if (student.getImie() != null) { //jeśli imie jest do zmiany to znaczy
                // że != null
                doEdycji.setImie(student.getImie()); // wstawiamy nową wartość
            }
            if (student.getNazwisko() != null) {
                doEdycji.setNazwisko(student.getNazwisko());
            }
            if (student.getPesel() != null) {
                doEdycji.setPesel(student.getPesel());
            }
            if (student.getIndeks() != null) {
                doEdycji.setIndeks(student.getIndeks());
            }

            return Optional.of(doEdycji);
        }
        return Optional.empty();
    }

    public List<Student> listaStudentow() {
        return list;
    }

    public List<Student> znajdzStudentaUrodzonegoW(String rok) {
        List<Student> listaUrodzonychWRoku = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getPesel().startsWith(rok)) {
                listaUrodzonychWRoku.add(list.get(i));
            }
        }
        return listaUrodzonychWRoku;
    }
}
