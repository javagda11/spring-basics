package com.sda.cwiczenia.service.bez_bazy;

import com.sda.cwiczenia.model.Dziennik;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DziennikService {

    private List<Dziennik> dziennikList = new ArrayList<>();

    public List<Dziennik> zwrocListe() {
        return dziennikList;
    }

    public Optional<Dziennik> znajdzDziennik(Long id) {
        for (int i = 0; i < dziennikList.size(); i++) {
            if (dziennikList.get(i).getId() == id) {
                return Optional.of(dziennikList.get(i));
            }
        }
        return Optional.empty();
    }

    public void dodajDziennik(Dziennik dziennik) {
        dziennikList.add(dziennik);
    }

    public boolean usunDziennik(Long id) {
        Optional<Dziennik> dziennik = znajdzDziennik(id);
        if(dziennik.isPresent()){
            dziennikList.remove(dziennik.get());
            return true;
        }

        return false;
    }
}
