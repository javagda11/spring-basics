package com.sda.cwiczenia.service;

import com.sda.cwiczenia.model.Dziennik;
import com.sda.cwiczenia.model.Student;
import com.sda.cwiczenia.repository.DziennikRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DziennikServiceBD {

    @Autowired
    private DziennikRepository dziennikRepository;

    @Autowired
    private StudentServiceBD studentServiceBD;

    public Dziennik dodajDziennik(Dziennik dziennik) {
        dziennik = dziennikRepository.save(dziennik);
        return dziennik;
    }

    public Optional<Dziennik> dodajStudentaDoDziennika(Long id_dziennik, Long id_student) {
        Optional<Dziennik> optionalDziennik = dziennikRepository.findById(id_dziennik);
        if (optionalDziennik.isPresent()) {
            // mamy dziennik, teraz szukamy studenta.

            Optional<Student> studentOptional = studentServiceBD.znajdzPoId(id_student);

            Dziennik dziennik = optionalDziennik.get(); // wyciągam dziennik z optional

            if (studentOptional.isPresent()) { // sprawdzam czy udało mi się znaleźć studenta
                Student student = studentOptional.get(); // wyciągam z optional

                dziennik.getListaUczniow().add(student); // dodaje studenta do dzinnika
                dziennik = dziennikRepository.save(dziennik); // zapisuje dziennik

                return Optional.of(dziennik);
            }
        }

        return Optional.empty();
    }
}
