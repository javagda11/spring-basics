package com.sda.cwiczenia.service;

import com.sda.cwiczenia.model.Student;
import com.sda.cwiczenia.model.dto.AddStudentDto;
import com.sda.cwiczenia.model.dto.EditStudentDto;
import com.sda.cwiczenia.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceBD {

    @Autowired
    private StudentRepository studentRepository;

    public List<Student> pobierzListeStudentow() {
        return studentRepository.findAll();
    }

    public Student zapiszStudenta(AddStudentDto dto) {
        Student student = AddStudentDto.createFromDto(dto);
        student = studentRepository.save(student);

        return student;
    }

    public Optional<Student> znajdzPoId(Long id) {
        return studentRepository.findById(id);
    }

    public boolean usunStudenta(Long id) {
        if (studentRepository.findById(id).isPresent()) {
            studentRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Optional<Student> szukajPoPeselu(String pesel) {
        return studentRepository.findByPesel(pesel);
    }

    public Optional<Student> aktualizuj(Long id, EditStudentDto edytowany) {
        Optional<Student> szukany = studentRepository.findById(id);
        if (szukany.isPresent()) {
            Student student1 = szukany.get();

            if (edytowany.getNazwisko() != null) {
                student1.setNazwisko(edytowany.getNazwisko());
            }
            if (edytowany.getImie() != null) {
                student1.setImie(edytowany.getImie());
            }
            student1 = studentRepository.save(student1);

            return Optional.of(student1);
        }
        return Optional.empty();
    }

    public void zapisz(Student student) {
        studentRepository.save(student);
    }
}





